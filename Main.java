/*
 * R.Schneider
 * Innere Klassen
 * 31.01.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/innerclass.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Main
 */

import java.util.*;
//package ;

public class Main
{
	public static void main(String[]arguments)
	{
		Aussen classtest=new Aussen();
		
		Aussen.Innen classtest_in=classtest.new Innen();
		
		classtest_in.seheAussen();
		
		System.out.println(classtest.macheRufeAnInnen());
		
		classtest.erzeugeInnenZwei();
		
		classtest.chaos();
		
		Aselder as=new Aselder();
		
		as.auslesen();
		
		as.anzeigen();
		
		Versuch av=new Versuch();
		
		//av.schreibe();
	}
}
