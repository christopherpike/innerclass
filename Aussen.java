/*
 * R.Schneider
 * Innere Klassen
 * 31.01.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/innerclass.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Aussen
 */

import java.util.*;
//package ;

public class Aussen
{
	private int x=7;
	
	public String macheRufeAnInnen()
	{
		Innen in=new Innen();
		return in.holeAussen();
	}



	class Innen
	{
		public void seheAussen()
		{
			System.out.println("X von Aussen hat den Wert von "+x+".");
		}
		
		public String holeAussen()
		{
			return "per hole: X von Aussen hat den Wert von "+x+".";
		}
	}
	
	public void erzeugeInnenZwei()
	{
		class InnenZwei
		{
			public void seheTiefer()
			{
				System.out.println("Ich sehe x mit dem Wert von "+x+", und noch tiefer");
			}
		}

		InnenZwei jikes=new InnenZwei();
		
		jikes.seheTiefer();
		
		
		
	}
	
	public void chaos()
	{
		//void frage()
		//{
			System.out.println("What?");
		//}

		//frage();
	}
	
	class Fleisch
	{
		public void schmor()
		{
			System.out.println("brutzel...");
		}
	}
	
	class Nahrung
	{
		Fleisch flesh=new Fleisch()
		{
			public void schmor()
			{
				System.out.println("brutzel..., aber anonym");
			}
			
			
		};
		
		Fleisch filet=new Fleisch()
		{
			IKochbar k=new IKochbar()
			{
				public void koche()
				{
					System.out.println("Ich koche");
				}
			};
		};
	}
	
	
	static class Versuch
	{
		static void schreibe()
		{
			System.out.println("Schreibe mal was.....");
		}
	}
	
	
	
}
//interface IKochbar
//{
//	public void koche();
//}

